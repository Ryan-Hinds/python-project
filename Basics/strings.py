# Allows the use of "\" without activating an escape character
print(r"c:\some\name")

a, b = 0, 1
while a < 10:
    print(a)
    a, b = b, a + b

i = 256 * 256
print("the value of i is: ", i)

while a < 1000:
    print(a, end=",")
    a, b = b, a + b