import random

# Conditional (ifm elif and else)
x = random.randint(-10, 42)
if x < 0:
    x = 0
    print("Negative set to zero")
elif x == 0:
    print("Zero")
elif x == 1:
    print("Single")
else:
    print(f"More {x}")

# For loops basics
words = ["cat", "window", "defenestrate", "Ministry"]
for w in words:
    print(w, len(w))

# Create sample collection (dict)
users = {"Hans": "active", "Calliaphe": "inactive", "Angelus": "active"}

# Strategy: Iterate over a copy
for user, status in users.copy().items():
    if status == "inactive":
        del users[user]

# Create new collection
active_users = {}
for user, status in users.copy().items():
    if status == "active":
        active_users[user] = status

# Using the range() function
for i in range(5):
    print(i)

# Using range with the list method
list_range = list(range(5, 10))
print(list_range)

# List range increment of 3
list_range = list(range(0, 10, 3))
print(list_range)

# Iterate over indices of a sequence using range() and len()
a = ["Mary", "had", "a", "little", "lamb"]
for i in range(len(a)):
    print(i, a[i])

# shows the numbers in range of the range() function using 10 as an example
print(range(10))

# Uses the sum method on a range of. in this case a range of 0 to 4(4 being excluded)
print(sum(range(4)))

# break, continue statements, and else clauses on loops.
for n in range(2, 10):
    for x in range(2, n):
        if n % x == 0:
            print(n, "equals", x, "*", n // x)
            break
        else:
            # Loop fell through without finding a factor
            print(n, "is a prime number")

for num in range(2, 10):
    if num % 2 == 0:
        print("Found even number", num)
        continue
    print("Found an odd number", num)

# Pass statement
# while True:
#     # This will make an infinite loop, to interrupt use ctrl + c to exit
#     pass

class MyEmptyClass:
    pass

def initlog(*args):
    # Remember to implement this!
    pass

# Match statements
# def http_error(status):
#     match status:
#         case 400:
#             return "Bad request"
#         case 404:
#             return "Not found"
#         case 418:
#             return "I am a teapot"
#         #You can combine several literals in a single pattern using "|" ("or"):
#         case 401 | 403| 404:
#             return "Not aloud"
#         case _:
#             return "Something is wrong with the internet"

# Match with "point" which is an (x, y) tuple
# match point:
#     case (0, 0):
#         print("Origin")
#     case (0, y):
#         print(f"Y = {y}")
#     case (x, 0):
#         print(f"X = {x}")
#     case (x, y):
#         print(f"X = {x}, Y = {y}")
#     # case _:
#     #     raise ValueError("Not a point")

# class Point:
#     x: int
#     y: int

#     def where_is(point):
#         match point:
#             case Point(x = 0, y = 0):
#                 print("Origin")
#             case Point(x = 0, y = y):
#                 print(f"Y = {y}")
#             case Point(x = x, y = 0):
#                 print(f"X = {x}")
#             case Point():
#                 print("Somewhere else")
#             # case _:
#             #     print("Not a point")

# match points:
#     case []:
#         print("No points")
#     case [Point(0, 0)]:
#         print("The origin")
#     case [Point(x, y)]:
#         print(f"Single point {x}, {y}")
#     case [Point(0, y1), Point(0, y2)]:
#         print(f"Two on the Y axis at {y1}, {y2}")
#     case _:
#         print("Something else")            

# match point:
#     case Point(x, y) if x == y:
#         print(f"Y=X at {x}")
#     case Point(x, y):
#         print(f"Not on the diagonal")

# Defining Functions
def fib(fn):
    """Print a Fibonacci series up to fn"""
    a, b = 0, 1
    while a < fn:
        print(a, end=" ")
        a, b = b, a + b
    print()

def fib2(n):
    """Return a list containing the fibonacci series up to n."""
    result = []
    a, b = 0, 1
    while a < n:
        result.append(a)
        a, b = b, a + b
    
    return result
