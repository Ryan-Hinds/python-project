# Using lists as queues
from collections import deque
from re import A
queue = deque(["Eric", "John", "Michael"])
# Terry arrives
queue.append("Terry")
# Graham arrives
queue.append("Graham")
# The first to arrive now leaves
queue.popleft()
# The second to arrive now leaves
queue.popleft()
# Prints the remaining in order of arrival
print(queue)

# List comprehension
squares = []
for x in range(10):
    squares.append(x ** 2)

print(squares)

# Using a lambda function
squares = list(map(lambda x: x ** 2, range(10)))
print(squares)

# Does the exact same as the two above using a different approach
squares = [x ** 2 for x in range(10)]
print(squares)

# The del statement
# Works similarly to the pop() method but it removes the index altogether. This can also be used on string slicing or list. It can also clear the entire list
# Example:
a = [-1, 1, 66.25, 333, 333, 1234.5]
print(a)
# Deletes index 0
del a[0]
print(a)
del a[2:4]
print(a)
# Delete it entire index
del a[:]
print(a)
# Delete the variable altogether
del a

# Onwards to tuples!
# Tuples are values separated by commas, but are not within any square brackets but to access values you need to just as you would a list
t = 12345, 54321, "Hello!"

# shows the first index of the tuple
print(t[0])